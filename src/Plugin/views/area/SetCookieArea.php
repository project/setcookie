<?php

namespace Drupal\setcookie\Plugin\views\area;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines an area plugin for setting a cookie.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("setcookie")
 */
class SetCookieArea extends AreaPluginBase {

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new SetCookieArea.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The currently active request object.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Request $request, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->request = $request;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['cookie_id'] = [
      'default' => '',
    ];
    $options['cookie_value'] = [
      'default' => '',
    ];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['cookie_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookie ID'),
      '#default_value' => $this->options['cookie_id'],
      '#description' => $this->t('The id for the cookie.'),
    ];
    $form['cookie_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookie value'),
      '#default_value' => $this->options['cookie_value'],
      '#description' => $this->t('The value for the cookie.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $this->request->cookies->set($this->options['cookie_id'], $this->options['cookie_value']);
    setcookie($this->options['cookie_id'], $this->options['cookie_value'], $this->time->getRequestTime() + 31536000, '/');

    $element = [
      '#attached' => [
        'library' => [
          'setcookie/setcookie',
        ],
        'drupalSettings' => [
          'setcookie' => [
            $this->options['cookie_id'] => $this->options['cookie_value'],
          ],
        ],
      ],
    ];
    $element['#cache']['contexts'][] = 'cookies:' . $this->options['cookie_id'];
    return $element;
  }

  public function isEmpty() {
    return FALSE;
  }

}
