<?php

/**
 * Implements hook_views_data().
 */
function setcookie_views_data() {
  $data['views']['setcookie'] = [
    'title' => t('Set a cookie'),
    'help' => t('Set a cookie when this view is rendered.'),
    'area' => [
      'id' => 'setcookie',
    ],
  ];
  return $data;
}
