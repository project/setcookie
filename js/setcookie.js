/**
 * @file
 * setcookie.js
 *
 * Defines the behavior of the setcookie module.
 */
(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.setcookie = {
    attach: function (context) {
      $.each(drupalSettings.setcookie, function( key, value ) {
        $.cookie(key, value, {path:'/'});
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
